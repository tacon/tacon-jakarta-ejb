package dev.tacon.jakarta.ejb.eao;

import java.io.Serializable;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import dev.tacon.annotations.NonNull;
import dev.tacon.annotations.Nullable;
import dev.tacon.jakartaee.persistence.entity.AbstractJpaEntity;
import dev.tacon.jakartaee.query.ISingleQuery;
import dev.tacon.jakartaee.query.Queries;
import jakarta.ejb.SessionContext;
import jakarta.persistence.EntityManager;

@SuppressWarnings("resource")
public abstract class ApplicationEAO {

	private static final Logger LOGGER = System.getLogger(ApplicationEAO.class.getName());

	public abstract EntityManager getEntityManager();

	public abstract SessionContext getSessionContext();

	public ISingleQuery<Void, Void> query() {
		return Queries.create(this.getEntityManager());
	}

	public <T> ISingleQuery<T, T> query(final @NonNull Class<T> entityClass) {
		return Queries.from(this.getEntityManager(), entityClass);
	}

	public <T extends AbstractJpaEntity<K>, K extends Serializable> T find(final @NonNull Class<T> entityClass, final @Nullable K id) {
		LOGGER.log(Level.DEBUG, () -> "find(" + entityClass + ", " + id + ")");
		return id != null ? this.getEntityManager().find(entityClass, id) : null;
	}

	public <T extends AbstractJpaEntity<K>, K extends Serializable> T getReference(final @NonNull Class<T> entityClass, final @Nullable K id) {
		LOGGER.log(Level.DEBUG, () -> "getReference(" + entityClass + ", " + id + ")");
		return id != null ? this.getEntityManager().getReference(entityClass, id) : null;
	}

	public <T> void insert(final T entity) {
		LOGGER.log(Level.DEBUG, () -> "insert " + entity);
		this.getEntityManager().persist(entity);
	}

	public <T> T update(final T entity) {
		LOGGER.log(Level.DEBUG, () -> "update " + entity);
		return this.getEntityManager().merge(entity);
	}

	public <T> void delete(final T entity) {
		LOGGER.log(Level.DEBUG, () -> "delete " + entity);
		this.getEntityManager().remove(this.getEntityManager().merge(entity));
	}
}
