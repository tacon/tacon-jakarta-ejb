package dev.tacon.jakarta.ejb.manager.handler;

import java.util.List;

import dev.tacon.interfaces.exception.ManagerException;
import dev.tacon.interfaces.exception.ManagerValidationException.ValidationError;
import dev.tacon.jakarta.interfaces.exception.EjbManagerValidationException;
import dev.tacon.jakarta.interfaces.validation.BeanValidator;
import dev.tacon.jakartaee.persistence.entity.AbstractJpaEntity;

public interface UpdateHandler<E extends AbstractJpaEntity<?>, D>
		extends UpdateValidator<E, D>, BeforeUpdateHandler<E, D>, AfterUpdateHandler<E, D> {

	@SuppressWarnings("rawtypes")
	UpdateHandler NO_OP = new UpdateHandler() {};

	@Override
	default void validateUpdate(final D dto) throws EjbManagerValidationException {
		final List<ValidationError> errors = BeanValidator.validate(dto);
		if (!errors.isEmpty()) {
			throw new EjbManagerValidationException(errors);
		}
	}

	@Override
	default void beforeUpdate(final D dto, final E entity) throws ManagerException {}

	@Override
	default void afterUpdate(final D dto, final E entity) throws ManagerException {}

	default BeforeInsertHandler<E, D> thenAlsoBeforeUpdate(final BeforeUpdateHandler<E, D> handler) {
		return (dto, entity) -> {
			this.beforeUpdate(dto, entity);
			handler.beforeUpdate(dto, entity);
		};
	}

	default AfterUpdateHandler<E, D> thenAlsoAfterUpdate(final AfterUpdateHandler<E, D> handler) {
		return (dto, entity) -> {
			this.afterUpdate(dto, entity);
			handler.afterUpdate(dto, entity);
		};
	}
}