package dev.tacon.jakarta.ejb.manager.handler;

import dev.tacon.interfaces.exception.ManagerException;
import dev.tacon.jakartaee.persistence.entity.AbstractJpaEntity;

public interface DeleteHandler<E extends AbstractJpaEntity<?>>
		extends BeforeDeleteHandler<E>, AfterDeleteHandler<E> {

	@SuppressWarnings("rawtypes")
	DeleteHandler NO_OP = new DeleteHandler() {};

	@Override
	default void beforeDelete(final E entity) throws ManagerException {}

	@Override
	default void afterDelete(final E entity) throws ManagerException {}

	default BeforeDeleteHandler<E> thenAlsoBeforeDelete(final BeforeDeleteHandler<E> handler) {
		return entity -> {
			this.beforeDelete(entity);
			handler.beforeDelete(entity);
		};
	}

	default AfterDeleteHandler<E> thenAlsoAfterDelete(final AfterDeleteHandler<E> handler) {
		return entity -> {
			this.afterDelete(entity);
			handler.afterDelete(entity);
		};
	}
}
