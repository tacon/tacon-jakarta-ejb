package dev.tacon.jakarta.ejb.manager.handler;

import dev.tacon.interfaces.exception.ManagerException;
import dev.tacon.jakartaee.persistence.entity.AbstractJpaEntity;

public class InheritableDeleteHandler<E extends AbstractJpaEntity<?>> implements DeleteHandler<E> {

	private final DeleteHandler<E> handler;

	public InheritableDeleteHandler(final DeleteHandler<E> handler) {
		this.handler = handler;
	}

	@Override
	public void beforeDelete(final E entity) throws ManagerException {
		this.handler.beforeDelete(entity);
	}

	@Override
	public void afterDelete(final E entity) throws ManagerException {
		this.handler.afterDelete(entity);
	}
}
