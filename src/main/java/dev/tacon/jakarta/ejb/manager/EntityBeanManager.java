package dev.tacon.jakarta.ejb.manager;

import java.io.Serializable;
import java.util.Optional;

import dev.tacon.annotations.NonNull;
import dev.tacon.annotations.Nullable;
import dev.tacon.interfaces.exception.ManagerException;
import dev.tacon.interfaces.exception.ManagerSecurityException;
import dev.tacon.jakarta.ejb.eao.ApplicationEAO;
import dev.tacon.jakarta.ejb.manager.handler.AfterDeleteHandler;
import dev.tacon.jakarta.ejb.manager.handler.AfterInsertHandler;
import dev.tacon.jakarta.ejb.manager.handler.AfterUpdateHandler;
import dev.tacon.jakarta.ejb.manager.handler.BeforeDeleteHandler;
import dev.tacon.jakarta.ejb.manager.handler.BeforeInsertHandler;
import dev.tacon.jakarta.ejb.manager.handler.BeforeUpdateHandler;
import dev.tacon.jakarta.ejb.manager.handler.DeleteHandler;
import dev.tacon.jakarta.ejb.manager.handler.InsertHandler;
import dev.tacon.jakarta.ejb.manager.handler.InsertValidator;
import dev.tacon.jakarta.ejb.manager.handler.UpdateHandler;
import dev.tacon.jakarta.ejb.manager.handler.UpdateValidator;
import dev.tacon.jakartaee.persistence.entity.AbstractJpaEntity;
import dev.tacon.jakartaee.query.Expressions;
import dev.tacon.jakartaee.query.ISingleQuery;
import dev.tacon.jakartaee.query.Queries;
import dev.tacon.jakartaee.query.criteria.IPredicate;
import dev.tacon.jakartaee.query.criteria.IRoot;

public abstract class EntityBeanManager<E extends AbstractJpaEntity<I>, I extends Serializable, D, P extends Serializable> {

	public EntityBeanManager() {}

	public @NonNull D insert(final @NonNull D dto) throws ManagerException {
		final InsertHandler<E, D> handler = this.createInsertHandler(this.getEao());
		return this.toDto(this.insert(dto, this.createSecurityManager(), handler, handler, handler));
	}

	protected @NonNull E insert(final @NonNull D dto, final EntitySecurityManager<E, P> securityManager, final @NonNull InsertValidator<D> validator, final @NonNull BeforeInsertHandler<E, D> beforeInsertHandler, final @NonNull AfterInsertHandler<E, D> afterInsertHandler) throws ManagerException {
		validator.validateInsert(dto);
		final E entity = this.toEntity(dto);
		if (!securityManager.hasInsertAccess(entity)) {

			throw new ManagerSecurityException();
		}
		beforeInsertHandler.beforeInsert(dto, entity);
		this.performInsert(entity);
		afterInsertHandler.afterInsert(dto, entity);
		return entity;
	}

	public void insert(final @NonNull E entity) throws ManagerException {
		if (!this.createSecurityManager().hasInsertAccess(entity)) {
			throw new ManagerSecurityException();
		}
		this.performInsert(entity);
	}

	protected void performInsert(final @NonNull E entity) throws ManagerException {
		this.getEao().insert(entity);
	}

	public void update(final @NonNull D dto) throws ManagerException {
		final UpdateHandler<E, D> handler = this.createUpdateHandler(this.getEao());
		this.update(dto, this.createSecurityManager(), handler, handler, handler);
	}

	public D updateAndGet(final @NonNull D dto) throws ManagerException {
		final UpdateHandler<E, D> handler = this.createUpdateHandler(this.getEao());
		final E updatedEntity = this.update(dto, this.createSecurityManager(), handler, handler, handler);
		return updatedEntity != null ? this.toDto(updatedEntity) : null;
	}

	protected E update(final @NonNull D dto, final EntitySecurityManager<E, P> securityManager, final @NonNull UpdateValidator<E, D> validator, final @NonNull BeforeUpdateHandler<E, D> beforeUpdateHandler, final @NonNull AfterUpdateHandler<E, D> afterUpdateHandler) throws ManagerException {
		validator.validateUpdate(dto);
		final E entity = this.findEntity(this.toId(dto), securityManager).orElse(null);
		if (entity == null) {
			beforeUpdateHandler.updateEntityNotFound();
			return null;
		}
		return this.updateInternal(dto, entity, securityManager, validator, beforeUpdateHandler, afterUpdateHandler);
	}

	public void update(final @NonNull D dto, final @NonNull E entity) throws ManagerException {
		final UpdateHandler<E, D> handler = this.createUpdateHandler(this.getEao());
		this.update(dto, entity, this.createSecurityManager(), handler, handler, handler);
	}

	protected @NonNull E update(final @NonNull D dto, final @NonNull E entity, final EntitySecurityManager<E, P> securityManager, final @NonNull UpdateValidator<E, D> validator, final @NonNull BeforeUpdateHandler<E, D> beforeUpdateHandler, final @NonNull AfterUpdateHandler<E, D> afterUpdateHandler) throws ManagerException {
		validator.validateUpdate(dto);
		return this.updateInternal(dto, entity, securityManager, validator, beforeUpdateHandler, afterUpdateHandler);
	}

	private @NonNull E updateInternal(final @NonNull D dto, final @NonNull E entity, final EntitySecurityManager<E, P> securityManager, final @NonNull UpdateValidator<E, D> validator, final @NonNull BeforeUpdateHandler<E, D> beforeUpdateHandler, final @NonNull AfterUpdateHandler<E, D> afterUpdateHandler) throws ManagerException {
		if (!securityManager.hasUpdateAccess(entity)) {
			throw new ManagerSecurityException();
		}
		validator.checkConcurrency(dto, entity);
		validator.validateAganistEntity(dto, entity);
		this.fillEntity(dto, entity);
		beforeUpdateHandler.beforeUpdate(dto, entity);
		if (!securityManager.hasUpdateAccess(entity)) {
			throw new ManagerSecurityException();
		}
		this.performUpdate(entity);
		afterUpdateHandler.afterUpdate(dto, entity);
		return entity;
	}

	public void update(final @NonNull E entity) throws ManagerException {
		if (!this.createSecurityManager().hasUpdateAccess(entity)) {
			throw new ManagerSecurityException();
		}
		this.performUpdate(entity);
	}

	protected void performUpdate(final @NonNull E entity) throws ManagerException {
		this.getEao().update(entity);
	}

	public void delete(final @NonNull I id) throws ManagerException {
		final DeleteHandler<E> handler = this.createDeleteHandler(this.getEao());
		this.delete(id, this.createSecurityManager(), handler, handler);
	}

	protected E delete(final @NonNull I id, final EntitySecurityManager<E, P> securityManager, final @NonNull BeforeDeleteHandler<E> beforeDeleteHandler, final @NonNull AfterDeleteHandler<E> afterDeleteHandler) throws ManagerException {
		final Optional<E> optEntity = this.findEntity(id, securityManager);
		if (!optEntity.isPresent()) {
			beforeDeleteHandler.deleteEntityNotFound();
			return null;
		}
		final E entity = optEntity.get();
		this.delete(entity, securityManager, beforeDeleteHandler, afterDeleteHandler);
		return entity;
	}

	protected void delete(final @NonNull E entity, final EntitySecurityManager<E, P> securityManager, final @NonNull BeforeDeleteHandler<E> beforeDeleteHandler, final @NonNull AfterDeleteHandler<E> afterDeleteHandler) throws ManagerException {
		if (!securityManager.hasDeleteAccess(entity)) {
			throw new ManagerSecurityException();
		}
		beforeDeleteHandler.beforeDelete(entity);
		this.performDelete(entity);
		afterDeleteHandler.afterDelete(entity);
	}

	public void delete(final @NonNull E entity) throws ManagerException {
		final DeleteHandler<E> handler = this.createDeleteHandler(this.getEao());
		this.delete(entity, this.createSecurityManager(), handler, handler);
	}

	protected void performDelete(final @NonNull E entity) throws ManagerException {
		this.getEao().delete(entity);
	}

	public @NonNull Optional<E> findEntity(final @Nullable I id) {
		return this.findEntity(id, this.createSecurityManager());
	}

	protected @NonNull Optional<E> findEntity(final @Nullable I id, final EntitySecurityManager<E, P> securityManager) {
		if (id == null) {
			return Optional.empty();
		}
		final E entity = this.getEao().find(this.getEntityClass(), id);
		if (entity != null && !securityManager.hasReadAccess(entity)) {
			throw new ManagerSecurityException();
		}
		return Optional.ofNullable(entity);
	}

	public D findDto(final @Nullable I id) {
		if (id == null) {
			return null;
		}
		return this.findEntity(id).map(this::toDto).orElse(null);
	}

	public E getEntityReference(final @Nullable I id) {
		return id != null ? this.getEao().getReference(this.getEntityClass(), id) : null;
	}

	protected abstract I toId(final @NonNull D dto);

	public @NonNull E toEntity(final @NonNull D dto) {
		final E entity = this.newEntity();
		this.fillEntityId(dto, entity);
		this.fillEntity(dto, entity);
		return entity;
	}

	protected @NonNull E newEntity() {
		try {
			return this.getEntityClass().getConstructor().newInstance();
		} catch (final ReflectiveOperationException ex) {
			throw new RuntimeException("Cannot find empty constructor for entity class: " + this.getEntityClass(), ex);
		}
	}

	protected void fillEntityId(final @NonNull D dto, final @NonNull E entity) {
		entity.setId(this.toId(dto));
	}

	protected abstract void fillEntity(final @NonNull D dto, final @NonNull E entity);

	public @NonNull D toDto(final @NonNull E entity) {
		final D dto = this.newDto(entity);
		ManagerInternals.fillDtoVersion(dto, entity);
		this.fillDto(dto, entity);
		return dto;
	}

	protected abstract @NonNull D newDto(@NonNull E entity);

	protected abstract void fillDto(@NonNull D dto, @NonNull E entity);

	public @NonNull ISingleQuery<E, E> query(final P params) {
		final IRoot<E> root = Expressions.from(this.getEntityClass());
		return this.query(root, params);
	}

	public @NonNull ISingleQuery<E, E> query(final @NonNull IRoot<E> root, final P params) {
		return this.query(root, this.createSecurityManager(), params);
	}

	protected @NonNull ISingleQuery<E, E> query(final @NonNull IRoot<E> root, final EntitySecurityManager<E, P> securityManager, final P params) {
		final IPredicate queryFilter = securityManager.prepareQueryFilter(root, params);
		@SuppressWarnings("resource")
		final ISingleQuery<E, E> query = Queries.create(this.getEao().getEntityManager())
				.from(root).select(this.getEntityClass(), root);
		if (queryFilter != null) {
			return query.where(queryFilter);
		}
		return query;
	}

	protected abstract @NonNull EntitySecurityManager<E, P> createSecurityManager();

	protected @NonNull @SuppressWarnings("unchecked") InsertHandler<E, D> createInsertHandler(final @NonNull ApplicationEAO eao) {
		if (this instanceof InsertHandler) {
			return (InsertHandler<E, D>) this;
		}
		return InsertHandler.NO_OP;
	}

	protected @NonNull @SuppressWarnings("unchecked") UpdateHandler<E, D> createUpdateHandler(final @NonNull ApplicationEAO eao) {
		if (this instanceof UpdateHandler) {
			return (UpdateHandler<E, D>) this;
		}
		return UpdateHandler.NO_OP;
	}

	protected @NonNull @SuppressWarnings("unchecked") DeleteHandler<E> createDeleteHandler(final @NonNull ApplicationEAO eao) {
		if (this instanceof DeleteHandler) {
			return (DeleteHandler<E>) this;
		}
		return DeleteHandler.NO_OP;
	}

	protected abstract @NonNull Class<E> getEntityClass();

	protected abstract @NonNull ApplicationEAO getEao();
}