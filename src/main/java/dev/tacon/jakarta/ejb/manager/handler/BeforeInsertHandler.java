package dev.tacon.jakarta.ejb.manager.handler;

import dev.tacon.interfaces.exception.ManagerException;
import dev.tacon.jakartaee.persistence.entity.AbstractJpaEntity;

public interface BeforeInsertHandler<E extends AbstractJpaEntity<?>, D> {

	void beforeInsert(D dto, E entity) throws ManagerException;
}
