package dev.tacon.jakarta.ejb.manager.handler;

import dev.tacon.interfaces.exception.ManagerException;
import dev.tacon.jakarta.interfaces.exception.EjbManagerValidationException;
import dev.tacon.jakartaee.persistence.entity.AbstractJpaEntity;

public class InheritableUpdateHandler<E extends AbstractJpaEntity<?>, D> implements UpdateHandler<E, D> {

	private final UpdateHandler<E, D> handler;

	public InheritableUpdateHandler(final UpdateHandler<E, D> handler) {
		this.handler = handler;
	}

	@Override
	public void validateUpdate(final D dto) throws EjbManagerValidationException {
		this.handler.validateUpdate(dto);
	}

	@Override
	public void beforeUpdate(final D dto, final E entity) throws ManagerException {
		this.handler.beforeUpdate(dto, entity);
	}

	@Override
	public void afterUpdate(final D dto, final E entity) throws ManagerException {
		this.handler.afterUpdate(dto, entity);
	}

	@Override
	public void validateAganistEntity(final D dto, final E entity) throws ManagerException {
		this.handler.validateAganistEntity(dto, entity);
	}
}
