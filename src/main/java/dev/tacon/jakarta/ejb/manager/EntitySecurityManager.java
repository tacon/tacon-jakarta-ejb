package dev.tacon.jakarta.ejb.manager;

import java.io.Serializable;
import java.util.function.BiFunction;
import java.util.function.Predicate;

import dev.tacon.annotations.NonNullByDefault;
import dev.tacon.annotations.Nullable;
import dev.tacon.jakartaee.persistence.entity.AbstractJpaEntity;
import dev.tacon.jakartaee.query.criteria.IPredicate;
import dev.tacon.jakartaee.query.criteria.IRoot;

@NonNullByDefault
public interface EntitySecurityManager<E extends AbstractJpaEntity<?>, P extends Serializable> {

	boolean hasReadAccess(E entity);

	boolean hasWriteAccess(E entity);

	default boolean hasInsertAccess(final E entity) {
		return this.hasWriteAccess(entity);
	}

	default boolean hasUpdateAccess(final E entity) {
		return this.hasWriteAccess(entity);
	}

	default boolean hasDeleteAccess(final E entity) {
		return this.hasWriteAccess(entity);
	}

	@Nullable
	IPredicate prepareQueryFilter(IRoot<E> root, @Nullable P params);

	static <E1 extends AbstractJpaEntity<?>, P1 extends Serializable> EntitySecurityManager<E1, P1> functional(
			final Predicate<E1> readAccessTest,
			final Predicate<E1> writeAccessTest,
			final BiFunction<IRoot<E1>, P1, IPredicate> queryFilterFactory) {
		return new EntitySecurityManager<>() {

			@Override
			public boolean hasReadAccess(final E1 entity) {
				return readAccessTest.test(entity);
			}

			@Override
			public boolean hasWriteAccess(final E1 entity) {
				return writeAccessTest.test(entity);
			}

			@Override
			public @Nullable IPredicate prepareQueryFilter(final IRoot<E1> root, final @Nullable P1 params) {
				return queryFilterFactory.apply(root, params);
			}
		};
	}

	static <E1 extends AbstractJpaEntity<?>, P1 extends Serializable> EntitySecurityManager<E1, P1> functional(
			final Predicate<E1> readAccessTest,
			final Predicate<E1> insertAccessTest,
			final Predicate<E1> updateAccessTest,
			final Predicate<E1> deleteAccessTest,
			final BiFunction<IRoot<E1>, P1, IPredicate> queryFilterFactory) {
		return new EntitySecurityManager<>() {

			@Override
			public boolean hasReadAccess(final E1 entity) {
				return readAccessTest.test(entity);
			}

			@Override
			public boolean hasInsertAccess(final E1 entity) {
				return insertAccessTest.test(entity);
			}

			@Override
			public boolean hasUpdateAccess(final E1 entity) {
				return updateAccessTest.test(entity);
			}

			@Override
			public boolean hasDeleteAccess(final E1 entity) {
				return deleteAccessTest.test(entity);
			}

			@Override
			public boolean hasWriteAccess(final E1 entity) {
				return false;
			}

			@Override
			public IPredicate prepareQueryFilter(final IRoot<E1> root, final @Nullable P1 params) {
				return queryFilterFactory.apply(root, params);
			}
		};
	}

	@SuppressWarnings("unchecked")
	static <E1 extends AbstractJpaEntity<?>, P1 extends Serializable> EntitySecurityManager<E1, P1> withoutSecurity() {
		return NO_SECURITY;
	}

	@SuppressWarnings("rawtypes")
	EntitySecurityManager NO_SECURITY = new EntitySecurityManager() {

		@Override
		public boolean hasReadAccess(final AbstractJpaEntity entity) {
			return true;
		}

		@Override
		public boolean hasWriteAccess(final AbstractJpaEntity entity) {
			return true;
		}

		@Override
		@Nullable
		public IPredicate prepareQueryFilter(final IRoot root, final @Nullable Serializable params) {
			return null;
		}
	};
}