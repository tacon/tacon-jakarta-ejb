package dev.tacon.jakarta.ejb.manager.handler;

import dev.tacon.interfaces.exception.ManagerException;
import dev.tacon.jakartaee.persistence.entity.AbstractJpaEntity;

public interface AfterDeleteHandler<E extends AbstractJpaEntity<?>> {

	void afterDelete(E entity) throws ManagerException;
}
