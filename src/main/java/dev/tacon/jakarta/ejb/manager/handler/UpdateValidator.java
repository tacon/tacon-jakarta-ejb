package dev.tacon.jakarta.ejb.manager.handler;

import java.io.Serializable;

import dev.tacon.interfaces.exception.ManagerConcurrencyException;
import dev.tacon.interfaces.exception.ManagerException;
import dev.tacon.interfaces.ext.HasVersion;
import dev.tacon.jakarta.ejb.manager.IncompatibileVersionTypeException;
import dev.tacon.jakarta.interfaces.exception.EjbManagerValidationException;
import dev.tacon.jakartaee.persistence.entity.AbstractJpaEntity;

public interface UpdateValidator<E extends AbstractJpaEntity<?>, D> {

	void validateUpdate(D dto) throws EjbManagerValidationException;

	default void checkConcurrency(final D dto, final E entity) throws ManagerException {
		checkVersion(dto, entity);
	}

	default void validateAganistEntity(final D dto, final E entity) throws ManagerException {
		// NO-OP by default
	}

	default UpdateValidator<E, D> withoutConcurrencyCheck() {
		return new UpdateValidator<>() {

			@Override
			public void validateUpdate(final D dto) throws EjbManagerValidationException {
				UpdateValidator.this.validateUpdate(dto);
			}

			@Override
			public void checkConcurrency(final D dto, final E entity) throws ManagerException {
				// prevent validation
			}

			@Override
			public void validateAganistEntity(final D dto, final E entity) throws ManagerException {
				UpdateValidator.this.validateAganistEntity(dto, entity);
			}
		};
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static void checkVersion(final Object dto, final Object entity) throws ManagerConcurrencyException {
		if (dto instanceof HasVersion && entity instanceof HasVersion) {
			final Comparable dtoVersion = (Comparable) ((HasVersion) dto).getVersion();
			final Comparable entityVersion = (Comparable) ((HasVersion) entity).getVersion();
			if (dtoVersion != null && entityVersion != null) {
				final int compareResult;
				try {
					compareResult = dtoVersion.compareTo(entityVersion);
				} catch (final ClassCastException ex) {
					throw new IncompatibileVersionTypeException(dtoVersion, entityVersion, ex);
				}
				if (compareResult != 0) {
					throw new ManagerConcurrencyException(dto.getClass().getName(), entity instanceof final AbstractJpaEntity<?> e ? e.getId().toString() : null, (Serializable) entityVersion, (Serializable) dtoVersion);
				}
			}
		}
	}
}
