package dev.tacon.jakarta.ejb.manager;

import java.io.Serializable;

import dev.tacon.interfaces.ext.HasVersion;

@SuppressWarnings({ "rawtypes", "unchecked" })
class ManagerInternals {

	static void fillDtoVersion(final Object dto, final Object entity) {
		if (dto instanceof HasVersion && entity instanceof HasVersion) {
			final Comparable entityVersion = (Comparable) ((HasVersion) entity).getVersion();
			try {
				((HasVersion) dto).setVersion((Serializable) entityVersion);
			} catch (final ClassCastException ex) {
				throw new IncompatibileVersionTypeException(entityVersion, ex);
			}
		}
	}

	private ManagerInternals() {}
}
