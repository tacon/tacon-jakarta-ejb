package dev.tacon.jakarta.ejb.manager.handler;

import dev.tacon.interfaces.exception.ManagerException;
import dev.tacon.jakartaee.persistence.entity.AbstractJpaEntity;

public interface BeforeDeleteHandler<E extends AbstractJpaEntity<?>> {

	default void deleteEntityNotFound() throws ManagerException {}

	void beforeDelete(E entity) throws ManagerException;
}
