package dev.tacon.jakarta.ejb.manager;

import dev.tacon.interfaces.ext.HasVersion;

/**
 * Exception thrown in case of incompatibile {@linkplain HasVersion} types.
 */
public class IncompatibileVersionTypeException extends RuntimeException {

	private static final long serialVersionUID = 3713962579821576341L;

	public IncompatibileVersionTypeException(final Object entityVersion, final Throwable cause) {
		super("Incompatibile type of HasVersion.getVersion() : " + entityVersion.getClass(), cause);
	}

	public IncompatibileVersionTypeException(final Object dtoVersion, final Object entityVersion, final Throwable cause) {
		super("Incompatibile types of HasVersion.getVersion() : "
				+ dtoVersion.getClass() + " vs " + entityVersion.getClass(), cause);
	}
}
