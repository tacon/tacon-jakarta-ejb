package dev.tacon.jakarta.ejb.manager.handler;

import dev.tacon.interfaces.exception.ManagerException;
import dev.tacon.jakartaee.persistence.entity.AbstractJpaEntity;

public interface AfterInsertHandler<E extends AbstractJpaEntity<?>, D> {

	void afterInsert(D dto, E entity) throws ManagerException;
}
