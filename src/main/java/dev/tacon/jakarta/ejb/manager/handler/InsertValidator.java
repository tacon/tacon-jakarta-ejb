package dev.tacon.jakarta.ejb.manager.handler;

import dev.tacon.interfaces.exception.ManagerException;

public interface InsertValidator<D> {

	void validateInsert(D dto) throws ManagerException;
}