package dev.tacon.jakarta.ejb.manager.handler;

import java.util.List;

import dev.tacon.interfaces.exception.ManagerException;
import dev.tacon.interfaces.exception.ManagerValidationException.ValidationError;
import dev.tacon.jakarta.interfaces.exception.EjbManagerValidationException;
import dev.tacon.jakarta.interfaces.validation.BeanValidator;
import dev.tacon.jakartaee.persistence.entity.AbstractJpaEntity;

public interface InsertHandler<E extends AbstractJpaEntity<?>, D>
		extends InsertValidator<D>, BeforeInsertHandler<E, D>, AfterInsertHandler<E, D> {

	@SuppressWarnings("rawtypes")
	InsertHandler NO_OP = new InsertHandler() {};

	@Override
	default void validateInsert(final D dto) throws EjbManagerValidationException {
		final List<ValidationError> errors = BeanValidator.validate(dto);
		if (!errors.isEmpty()) {
			throw new EjbManagerValidationException(errors);
		}
	}

	@Override
	default void beforeInsert(final D dto, final E entity) throws ManagerException {}

	@Override
	default void afterInsert(final D dto, final E entity) throws ManagerException {}

	default BeforeInsertHandler<E, D> thenAlsoBeforeInsert(final BeforeInsertHandler<E, D> handler) {
		return (dto, entity) -> {
			this.beforeInsert(dto, entity);
			handler.beforeInsert(dto, entity);
		};
	}

	default AfterInsertHandler<E, D> thenAlsoAfterInsert(final AfterInsertHandler<E, D> handler) {
		return (dto, entity) -> {
			this.afterInsert(dto, entity);
			handler.afterInsert(dto, entity);
		};
	}
}