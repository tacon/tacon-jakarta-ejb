package dev.tacon.jakarta.ejb.manager;

import java.io.Serializable;
import java.util.UUID;

import dev.tacon.annotations.NonNull;
import dev.tacon.interfaces.dto.DtoWithUuid;
import dev.tacon.interfaces.reference.DtoUuidRef;
import dev.tacon.jakartaee.persistence.entity.AbstractJpaEntity;

public abstract class UuidEntityDtoManager<E extends AbstractJpaEntity<UUID>, D extends DtoWithUuid<D>, P extends Serializable>
		extends EntityDtoManager<E, UUID, D, UUID, DtoUuidRef<D>, P> {

	@Override
	protected @NonNull DtoUuidRef<D> newReference(final D dto) {
		return new DtoUuidRef<>(UUID.randomUUID());
	}

	@Override
	protected @NonNull UUID toId(final DtoUuidRef<D> reference) {
		return reference.getId();
	}

	@Override
	protected @NonNull DtoUuidRef<D> toReference(final @NonNull UUID id) {
		return new DtoUuidRef<>(id);
	}
}
