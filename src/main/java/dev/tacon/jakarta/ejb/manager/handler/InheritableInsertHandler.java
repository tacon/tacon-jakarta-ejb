package dev.tacon.jakarta.ejb.manager.handler;

import dev.tacon.interfaces.exception.ManagerException;
import dev.tacon.jakarta.interfaces.exception.EjbManagerValidationException;
import dev.tacon.jakartaee.persistence.entity.AbstractJpaEntity;

public class InheritableInsertHandler<E extends AbstractJpaEntity<?>, D> implements InsertHandler<E, D> {

	private final InsertHandler<E, D> handler;

	public InheritableInsertHandler(final InsertHandler<E, D> handler) {
		this.handler = handler;
	}

	@Override
	public void validateInsert(final D dto) throws EjbManagerValidationException {
		this.handler.validateInsert(dto);
	}

	@Override
	public void beforeInsert(final D dto, final E entity) throws ManagerException {
		this.handler.beforeInsert(dto, entity);
	}

	@Override
	public void afterInsert(final D dto, final E entity) throws ManagerException {
		this.handler.afterInsert(dto, entity);
	}
}
