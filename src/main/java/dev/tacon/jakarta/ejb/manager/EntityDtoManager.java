package dev.tacon.jakarta.ejb.manager;

import java.io.Serializable;
import java.util.Optional;

import dev.tacon.annotations.NonNull;
import dev.tacon.annotations.Nullable;
import dev.tacon.interfaces.dto.DtoWithId;
import dev.tacon.interfaces.exception.ManagerException;
import dev.tacon.interfaces.reference.DtoRef;
import dev.tacon.jakarta.ejb.manager.handler.AfterDeleteHandler;
import dev.tacon.jakarta.ejb.manager.handler.BeforeDeleteHandler;
import dev.tacon.jakarta.ejb.manager.handler.DeleteHandler;
import dev.tacon.jakartaee.persistence.entity.AbstractJpaEntity;

public abstract class EntityDtoManager<E extends AbstractJpaEntity<I>, I extends Serializable, D extends DtoWithId<?, K, R>, K extends Serializable, R extends DtoRef<? super D, K>, P extends Serializable>
		extends EntityBeanManager<E, I, D, P> {

	public EntityDtoManager() {}

	public void delete(final @NonNull R reference) throws ManagerException {
		final DeleteHandler<E> handler = this.createDeleteHandler(this.getEao());
		this.delete(reference, this.createSecurityManager(), handler, handler);
	}

	protected E delete(final @NonNull R reference, final EntitySecurityManager<E, P> securityManager, final @NonNull BeforeDeleteHandler<E> beforeDeleteHandler, final @NonNull AfterDeleteHandler<E> afterDeleteHandler) throws ManagerException {
		return super.delete(this.toId(reference), securityManager, beforeDeleteHandler, afterDeleteHandler);
	}

	public @NonNull Optional<E> findEntity(final @Nullable D dto) {
		return dto != null ? this.findEntity(dto.getRef()) : Optional.empty();
	}

	public @NonNull Optional<E> findEntity(final @Nullable R reference) {
		return reference != null ? this.findEntity(this.toId(reference)) : Optional.empty();
	}

	public D findDto(final @Nullable R reference) {
		if (reference == null) {
			return null;
		}
		return this.findEntity(reference).<D> map(this::toDto).orElse(null);
	}

	public E getEntityReference(final @Nullable R reference) {
		return reference != null ? this.getEntityReference(this.toId(reference)) : null;
	}

	protected abstract @NonNull I toId(final R reference);

	@Override
	protected final @NonNull I toId(final @NonNull D dto) {
		return this.toId(dto.getRef());
	}

	@Override
	protected void fillEntityId(final @NonNull D dto, final @NonNull E entity) {
		R reference = dto.getRef();
		if (reference == null) {
			reference = this.newReference(dto);
		}
		entity.setId(this.toId(reference));
	}

	protected R newReference(final D dto) {
		return null;
	}

	public R toReference(final @Nullable E entity) {
		return entity != null ? this.toReference(entity.getId()) : null;
	}

	protected abstract @NonNull R toReference(@NonNull I id);
}