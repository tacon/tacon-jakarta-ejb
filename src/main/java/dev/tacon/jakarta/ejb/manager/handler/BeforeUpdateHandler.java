package dev.tacon.jakarta.ejb.manager.handler;

import dev.tacon.interfaces.exception.ManagerException;
import dev.tacon.jakartaee.persistence.entity.AbstractJpaEntity;

public interface BeforeUpdateHandler<E extends AbstractJpaEntity<?>, D> {

	default void updateEntityNotFound() throws ManagerException {}

	void beforeUpdate(D dto, E entity) throws ManagerException;
}